package main

import (
	"crypto/rand"
	"crypto/rsa"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Token struct {
	Token string `json:"token"`
}

type LoginCredentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

const (
	privKeyPath = "keys/app.rsa"
	pubKeyPath  = "keys/app.rsa.pub"
)

var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)
var verifyBytes, signBytes []byte

func LoginHandler(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)

	var data LoginCredentials

	err := decoder.Decode(&data)
	if err != nil {
		panic(err)
		respondWithError(w, http.StatusForbidden, "Credentials decoding error.")
		return
	}

	log.Println(data)

	user := User{Email: data.Email}

	err = user.findByEmail(app.DB)

	if err != nil {
		fmt.Println("Invalid email.")
		fmt.Println(err.Error())

		respondWithError(w, http.StatusForbidden, "User not found.")

		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data.Password))

	if err != nil {
		fmt.Println("Invalid password.")
		fmt.Println(err.Error())

		respondWithError(w, http.StatusForbidden, "Wrong password.")

		return
	}


	//create a token instance using the token string
	generatedToken := generateToken()

	user.RememberToken = generatedToken
	user.updateToken(app.DB)

	response := Token{generatedToken}
	respondWithJSON(w, http.StatusOK, response)

}

func RegisterHandler(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)

	var data User

	err := decoder.Decode(&data)
	if err != nil {
		panic(err)
		respondWithError(w, http.StatusForbidden, "Passed data decoding error.")
		return
	}

	var emailFromDB string
	err = app.DB.QueryRow("SELECT email FROM users WHERE email=?", data.Email).Scan(&emailFromDB)

	if emailFromDB != "" {
		respondWithError(w, http.StatusForbidden, "Email in use!")
		log.Println("Email in use!")
		return
	}

	switch {

	case err == sql.ErrNoRows:

		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)

		if err != nil {
			http.Error(w, "Server error, unable to create your account.", 500)
			respondWithError(w, http.StatusForbidden, "Server error.")
			log.Println(err.Error())
			return
		}

		user := User{Email: data.Email, Password: string(hashedPassword), FirstName: data.FirstName, LastName: data.LastName}
		user.Role.ID = 2

		validationErrors := user.validate()

		if data.Password == "" { validationErrors.Add("password", "Password is required!") }

		if len(validationErrors) > 0 {
			validationFailResponse(w, validationErrors)
			return
		}

		err = user.createUser(app.DB)

		if err != nil {
			respondWithError(w, http.StatusInternalServerError, "Server error, unable to create your account.")
			log.Println(err.Error())
			return
		}

		w.Write([]byte("User created!"))
		return
	case err != nil:
		respondWithError(w, http.StatusInternalServerError, "Server error, unable to create your account.")
		log.Println(err.Error())
		return
	default:
		respondWithError(w, http.StatusInternalServerError, "Unknown error.")
		log.Println(err.Error())
	}
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "User not found")
		return
	}

	log.Println(user)

	if err = user.removeToken(app.DB); err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Server error")
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})

}

func ValidateTokenMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	//validate token
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})

	if err == nil {

		if token.Valid {
			next(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(w, "Token is not valid")
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Unauthorised access to this resource")
	}

}

func getTokenFromRequest(r *http.Request) string {

	tokens, ok := r.Header["Authorization"]

	var token string

	if ok && len(tokens) >= 1 {
		token = tokens[0]
		token = strings.TrimPrefix(token, "Bearer ")

		return token
	}

	return ""
}

func getUserFromToken(r *http.Request) (User, error) {

	token := getTokenFromRequest(r)
	if token == "" {
		return User{}, fmt.Errorf("token error")
	}

	user := User{RememberToken: token}

	if err := user.getUserByToken(app.DB); err != nil {
		return User{}, err
	}

	return user, nil

}

func userMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	log.Println("User: " + user.Email)

	next(w, r)

}

func adminMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	if user.Role.ID == 1 {
		next(w, r)
	} else {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
	}

	return
}

func initKeys() {
	var err error

	signBytes, err = ioutil.ReadFile(privKeyPath)
	if err != nil {
		log.Fatalf("Error reading private key: %v", err)
	}
	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		log.Fatalf("Error parsing private key: %v", err)
	}
	verifyBytes, err = ioutil.ReadFile(pubKeyPath)
	if err != nil {
		log.Fatalf("Error reading public key: %v", err)
	}
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		log.Fatalf("Error parsing public key: %v", err)
	}
}

func generateToken() string {
	b := make([]byte, 64)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
