package main

import (
	"encoding/json"
	"net/http"
	"net/url"
)

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func validationFailResponse(w http.ResponseWriter, errors url.Values) {
	if len(errors) > 0 {
		err := map[string]interface{}{"error": errors}
		respondWithJSON(w, http.StatusBadRequest, err)
	}
}

func filterUserData(u *User) RESTUser {
	user := RESTUser{}
	user.ID = u.ID
	user.Role = u.Role.Name
	user.Email = u.Email
	user.FirstName = u.FirstName
	user.LastName = u.LastName

	return user
}
