package main

import (
	"database/sql"
	"fmt"
	"net/url"
)

type Location struct {
	ID			int 	 `json:"id"`
	User		RESTUser `json:"user"`
	Latitude    float32  `json:"latitude"`
	Longitude   float32  `json:"longitude"`
	CreatedAt	string 	 `json:"created_at"`
}

type DBLocation struct {
	ID			int 	 `json:"id"`
	Latitude    float32  `json:"latitude"`
	Longitude   float32  `json:"longitude"`
	CreatedAt	string 	 `json:"created_at"`
}

func (l *Location) createLocation(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO locations(user_id, latitude, longitude) VALUES(?, ?, ?)")
	_, err := db.Exec(statement, l.User.ID, l.Latitude, l.Longitude)
	if err != nil { return err }

	err = db.QueryRow(`SELECT LAST_INSERT_ID()`).Scan(&l.ID)
	if err != nil { return err }

	err = db.QueryRow("SELECT created_at FROM locations WHERE id=?", l.ID).Scan(&l.CreatedAt)
	if err != nil { return err }

	return nil
}

func (l *Location) getLocation(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT user_id, latitude, longitude, created_at FROM locations WHERE id=?")
	return db.QueryRow(statement, l.ID).Scan(&l.User.ID, &l.Latitude, &l.Longitude, &l.CreatedAt)
}

func (l *Location) deleteLocation(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM locations WHERE id=?")
	_, err := db.Exec(statement, l.ID)
	return err
}

func (l *Location) validate() url.Values {
	errs := url.Values{}

	if l.Latitude == 0 {
		errs.Add("latitude", "The latitude is required!")
	}

	if l.Longitude == 0 {
		errs.Add("longitude", "The longitude is required!")
	}

	return errs
}

func getUserLocations(db *sql.DB, id int) (*sql.Rows, error) {
	statement := fmt.Sprintf("SELECT id, latitude, longitude, created_at FROM locations WHERE user_id=? AND created_at >= DATE(NOW())")
	return db.Query(statement, id)
}