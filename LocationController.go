package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"strconv"
	"database/sql"
	"encoding/json"
)

func (a *App) createLocation(w http.ResponseWriter, r *http.Request) {

	var dbLocation DBLocation

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&dbLocation); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	defer r.Body.Close()

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	l := new(Location)
	l.Longitude = dbLocation.Longitude
	l.Latitude = dbLocation.Latitude
	l.CreatedAt = dbLocation.CreatedAt
	l.User = filterUserData(&user)

	validationErrors := l.validate()
	if len(validationErrors) > 0 {
		validationFailResponse(w, validationErrors)
		return
	}

	if err := l.createLocation(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, l)
}

func (a *App) getLocation(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	l := Location{ID: id}

	if err := l.getLocation(app.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	user := User{ID:l.User.ID}
	if err := user.getUser(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, "user not found")
		return
	}

	l.User = filterUserData(&user)

	respondWithJSON(w, http.StatusOK, l)
}

func (a *App) deleteLocation(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	l := Location{ID: id}

	if err = l.getLocation(app.DB); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	if err := l.deleteLocation(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func (a *App) getUserLocations(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	locations := []Location{}

	rows, err := getUserLocations(app.DB, id)

	if err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	for rows.Next() {
		var l Location

		if err := rows.Scan(&l.ID, &l.Latitude, &l.Longitude, &l.CreatedAt); err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		user := User{ID:id}

		if err = user.getUser(app.DB); err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		l.User = filterUserData(&user)

		locations = append(locations, l)
	}

	respondWithJSON(w, http.StatusOK, locations)
}
