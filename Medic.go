package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
)

type Medic struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Image        string `json:"image"`
	Brochure     string `json:"brochure"`
	WholesalerID int    `json:"wholesaler_id"`
}

type Wholesaler struct {
	ID     int     `json:"id"`
	Name   string  `json:"name"`
	Image  string  `json:"image"`
	Medics []Medic `json:"medics"`
}

func (w *Wholesaler) createWholesaler(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO wholesalers(name, image) VALUES(?, ?)")
	_, err := db.Exec(statement, w.Name, w.Image)

	if err != nil {
		return err
	}

	err = db.QueryRow(`SELECT LAST_INSERT_ID()`).Scan(&w.ID)

	if err != nil {
		return err
	}

	return nil
}

func (w *Wholesaler) getWholesaler(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT name, image FROM wholesalers WHERE id=?")
	return db.QueryRow(statement, w.ID).Scan(&w.Name, &w.Image)
}

func (w *Wholesaler) updateWholesaler(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE wholesalers SET name=?, image=? WHERE id=?")
	_, err := db.Exec(statement, w.Name, w.Image, w.ID)
	return err
}

func (w *Wholesaler) deleteWholesaler(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM wholesalers WHERE id=?")
	_, err := db.Exec(statement, w.ID)
	return err
}

func getWholesalers(db *sql.DB) ([]Wholesaler, error) {

	statement := fmt.Sprintf("SELECT id, name, image FROM wholesalers")
	rows, err := db.Query(statement)

	defer rows.Close()

	if err != nil {
		log.Printf(err.Error())
		return nil, err
	}

	wholesalers := []Wholesaler{}

	for rows.Next() {

		var w Wholesaler

		err := rows.Scan(&w.ID, &w.Name, &w.Image)

		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		wholesalers = append(wholesalers, w)
	}

	return wholesalers, nil
}

func getWholesalersAndMedics(db *sql.DB) ([]Wholesaler, error) {

	statement := fmt.Sprintf("SELECT id, name, image FROM wholesalers")
	rows, err := db.Query(statement)

	defer rows.Close()

	if err != nil {
		log.Printf(err.Error())
		return nil, err
	}

	wholesalers := []Wholesaler{}

	for rows.Next() {

		var w Wholesaler

		err := rows.Scan(&w.ID, &w.Name, &w.Image)

		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		err = w.getWholesalerMedics(db)

		if err != nil {
			log.Printf(err.Error())
			return nil, err
		}

		wholesalers = append(wholesalers, w)
	}

	if err != nil {
		fmt.Sprintf(err.Error())
		return nil, err
	}

	return wholesalers, nil
}

func (w *Wholesaler) getWholesalerMedics(db *sql.DB) error {

	statement := fmt.Sprintf("SELECT id, name, description, image, brochure FROM medics WHERE wholesaler_id = ? AND is_deleted = 0")
	items, err := db.Query(statement, w.ID)

	defer items.Close()

	if err != nil {
		fmt.Sprintf(err.Error())
		return err
	}

	for counter := 0; items.Next(); counter++ {

		var medic Medic

		err := items.Scan(&medic.ID, &medic.Name, &medic.Description, &medic.Image, &medic.Brochure)

		if err != nil {
			log.Fatal(err)
			return err
		}

		medic.WholesalerID = w.ID

		w.Medics = append(w.Medics, medic)

	}

	return nil
}

/////////////////
//	MEDIC CRUD
/////////////////

func (m *Medic) createMedicForWholesaler(db *sql.DB, wholesalerID int) error {
	statement := fmt.Sprintf("INSERT INTO medics(name, description, image, brochure, wholesaler_id) VALUES(?, ?, ?, ? ,?)")
	_, err := db.Exec(statement, m.Name, m.Description, m.Image, m.Brochure, wholesalerID)

	if err != nil { return err }

	err = db.QueryRow(`SELECT LAST_INSERT_ID()`).Scan(&m.ID)

	if err != nil { return err }

	return nil
}

func (m *Medic) getMedic(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT name, description, image, brochure, wholesaler_id FROM medics WHERE id=? AND is_deleted = 0")
	return db.QueryRow(statement, m.ID).Scan(&m.Name, &m.Description, &m.Image, &m.Brochure, &m.WholesalerID)
}

func (m *Medic) updateMedic(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE medics SET name=?, description=?, image=?, brochure=?, wholesaler_id=? WHERE id=?")
	_, err := db.Exec(statement, m.Name, m.Description, m.Image, m.Brochure, m.WholesalerID, m.ID)
	return err
}

func (m *Medic) deleteMedic(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE medics SET is_deleted=true WHERE id=?")
	_, err := db.Exec(statement, m.ID)
	return err
}

func (m *Medic) validate() url.Values {
	errs := url.Values{}

	if m.Name == "" {
		errs.Add("name", "Name is required!")
	}

	if m.Description == "" {
		errs.Add("description", "Description is required!")
	}

	if m.WholesalerID == 0 {
		errs.Add("wholesaler_id", "Wholesaler ID is required!")
	}

	return errs
}
