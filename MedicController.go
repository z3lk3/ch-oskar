package main

import (
	"database/sql"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"log"
	"net/http"
	"strconv"
)

func (a *App) getWholesaler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")

		return
	}

	wholesaler := Wholesaler{ID: id}

	if err := wholesaler.getWholesaler(app.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	respondWithJSON(w, http.StatusOK, wholesaler)

}

func (a *App) getMedicsAndWholesalers(w http.ResponseWriter, r *http.Request) {

	medics, err := getWholesalersAndMedics(a.DB)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, medics)
}

func (a *App) getWholesalers(w http.ResponseWriter, r *http.Request) {

	wholesalers, err := getWholesalers(a.DB)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, wholesalers)
}

func (a *App) createWholesaler(w http.ResponseWriter, r *http.Request) {

	wholesaler := new(Wholesaler)

	// DECODING FORM DATA
	err := r.ParseMultipartForm(0)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	decoder := schema.NewDecoder()

	err = decoder.Decode(wholesaler, r.PostForm)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	// UPLOADING FILE
	var filename = ""
	file, handle, err := r.FormFile("image")

	if err != nil {
		log.Println(err.Error())
	} else {

		defer file.Close()
		filename = getAndUploadFileFromRequest(w, file, handle)

	}

	if filename != "" {
		wholesaler.Image = filename
	} else {
		wholesaler.Image = "placeholder.jpg"
	}

	if err := wholesaler.createWholesaler(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, wholesaler)
	return
}

func (a *App) updateWholesaler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	wholesaler := new(Wholesaler)
	wholesaler.ID = id

	if err = wholesaler.getWholesaler(app.DB); err != nil {
		print(err.Error())
	}

	oldImage := wholesaler.Image

	// DECODING FORM DATA
	err = r.ParseMultipartForm(0)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload1")
		return
	}

	decoder := schema.NewDecoder()

	err = decoder.Decode(wholesaler, r.PostForm)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload2")
		return
	}

	// UPLOADING FILE
	var filename = ""
	file, handle, err := r.FormFile("image")

	if err != nil {
		log.Println(err.Error())
	} else {

		defer file.Close()
		filename = getAndUploadFileFromRequest(w, file, handle)

	}

	if filename != "" {
		wholesaler.Image = filename
	} else {
		wholesaler.Image = oldImage
	}

	if err := wholesaler.updateWholesaler(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, wholesaler)
}

func (a *App) deleteWholesaler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	wholesaler := Wholesaler{ID: id}

	if err = wholesaler.getWholesaler(a.DB); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	if err := wholesaler.deleteWholesaler(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

//////////////
// MEDIC
//////////////

func (a *App) getMedicByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")

		return
	}

	medic := Medic{ID: id}

	if err := medic.getMedic(a.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Medic not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	respondWithJSON(w, http.StatusOK, medic)
}

func (a *App) getMedicsByWholesalerID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")

		return
	}

	wholesaler := Wholesaler{ID: id}

	if err := wholesaler.getWholesalerMedics(a.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Medics not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	respondWithJSON(w, http.StatusOK, wholesaler.Medics)
}

func (a *App) createMedic(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	wholesalerID, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	wholesaler := Wholesaler{ID: wholesalerID}

	if err = wholesaler.getWholesaler(app.DB); err != nil {
		respondWithError(w, http.StatusBadRequest, "Not found")
		return
	}

	medic := new(Medic)
	medic.WholesalerID = wholesalerID

	// DECODING FORM DATA
	err = r.ParseMultipartForm(0)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	decoder := schema.NewDecoder()

	err = decoder.Decode(medic, r.PostForm)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	// UPLOADING IMAGE
	var filename = ""
	file, handle, err := r.FormFile("image")

	if err != nil {
		log.Println(err.Error())
	} else {

		defer file.Close()
		filename = getAndUploadFileFromRequest(w, file, handle)

	}

	if filename != "" {
		medic.Image = filename
	} else {
		medic.Image = "placeholder.jpg"
	}

	// UPLOADING BROCHURE
	brochureName := ""
	brochure, brochureHandle, err := r.FormFile("brochure")
	if err != nil {
		log.Println(err.Error())
	} else {

		defer file.Close()
		brochureName = getAndUploadFileFromRequest(w, brochure, brochureHandle)

	}

	if brochureName != "" {
		medic.Brochure = brochureName
	} else {
		medic.Brochure = "placeholder.jpg"
	}

	validationErrors := medic.validate()
	if len(validationErrors) > 0 {
		validationFailResponse(w, validationErrors)
		return
	}

	if err = medic.createMedicForWholesaler(app.DB, medic.WholesalerID); err != nil {
		respondWithError(w, http.StatusInternalServerError, "Can't create item")
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func (a *App) updateMedic(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	wholesalerID, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	medicID, err := strconv.Atoi(vars["medicid"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	medic := new(Medic)
	medic.ID = medicID
	medic.WholesalerID = wholesalerID

	if err = medic.getMedic(app.DB); err != nil {
		respondWithError(w, http.StatusBadRequest, "Not found")
		return
	}

	oldImage := medic.Image

	// DECODING FORM DATA
	err = r.ParseMultipartForm(0)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	decoder := schema.NewDecoder()

	err = decoder.Decode(medic, r.PostForm)
	if err != nil {
		log.Println(err.Error())
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	// UPLOADING IMAGE
	var filename = ""
	file, handle, err := r.FormFile("image")

	if err != nil {
		log.Println(err.Error())
	} else {

		defer file.Close()
		filename = getAndUploadFileFromRequest(w, file, handle)

	}

	if filename != "" {
		medic.Image = filename
	} else {
		medic.Image = oldImage
	}

	// UPLOADING BROCHURE
	brochureName := ""
	brochure, brochureHandle, err := r.FormFile("brochure")
	if err != nil {
		log.Println(err.Error())
	} else {

		defer file.Close()
		brochureName = getAndUploadFileFromRequest(w, brochure, brochureHandle)

	}

	if brochureName != "" {
		medic.Brochure = brochureName
	} else {
		medic.Brochure = "placeholder.jpg"
	}

	if err := medic.updateMedic(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, "update error")
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func (a *App) deleteMedic(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	medicID, err := strconv.Atoi(vars["medicid"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	medic := new(Medic)
	medic.ID = medicID

	if err = medic.deleteMedic(app.DB); err != nil {
		respondWithError(w, http.StatusBadRequest, "Not found")
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}
