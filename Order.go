package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
)

type Order struct {
	ID            int            `json:"id"`
	Pharmacy      Pharmacy       `json:"pharmacy"`
	User          RESTUser       `json:"user"`
	OrderedMedics []OrderedMedic `json:"medics"`
	Note          string         `json:"note"`
	ContactPerson string         `json:"contact_person"`
	Processed     bool           `json:"processed"`
}

type OrderedMedic struct {
	Medic    Medic `json:"medic"`
	Quantity int   `json:"quantity"`
}

type DBOrder struct {
	ID            int    `json:"id"`
	PharmacyID    int    `json:"pharmacy_id"`
	UserID        int    `json:"user_id"`
	Note          string `json:"note"`
	ContactPerson string `json:"contact_person"`
	Processed     bool   `json:"processed"`
}

func (o *Order) getOrder(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT pharmacy_id, user_id, note, contact_person, processed FROM orders WHERE id=?")
	return db.QueryRow(statement, o.ID).Scan(&o.Pharmacy.ID, &o.User.ID, &o.Note, &o.ContactPerson, &o.Processed)
}

func (o *Order) deleteOrder(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM orders WHERE id=?")
	_, err := db.Exec(statement, o.ID)
	return err
}

func getOrders(db *sql.DB) (*sql.Rows, error) {
	statement := fmt.Sprintf("SELECT id, pharmacy_id, user_id, note, contact_person, processed FROM orders WHERE processed=0")
	return db.Query(statement)
}

func (o *Order) getOrderMedics(db *sql.DB) error {

	statement := fmt.Sprintf("SELECT m.id, m.name, m.description, m.image, m.brochure, m.wholesaler_id, om.quantity FROM medics m JOIN order_medic om ON m.id = om.medic_id WHERE om.order_id=?")
	rows, err := db.Query(statement, o.ID)

	if err != nil {
		log.Printf(err.Error())
		return nil
	}

	orderedMedics := []OrderedMedic{}

	for rows.Next() {

		var m OrderedMedic

		err := rows.Scan(&m.Medic.ID, &m.Medic.Name, &m.Medic.Description, &m.Medic.Image, &m.Medic.Brochure, &m.Medic.WholesalerID, &m.Quantity)

		if err != nil {
			log.Println(err)
			return nil
		}

		orderedMedics = append(orderedMedics, m)

	}

	o.OrderedMedics = orderedMedics

	return nil
}

func (o *Order) validate() url.Values {
	errs := url.Values{}

	if o.Pharmacy.ID == 0 {
		errs.Add("pharmacy_id", "Pharmacy ID is required!")
	}

	if o.User.ID == 0 {
		errs.Add("user_id", "User ID is required!")
	}

	if o.ContactPerson == "" {
		errs.Add("contact_person", "Contact person is required!")
	}

	return errs
}