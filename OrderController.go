package main

import (
	"database/sql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func (a *App) getOrder(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	order := Order{ID: id}

	if err := order.getOrder(app.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	err = order.getOrderMedics(app.DB)

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Medics not found")
		return
	}

	user := User{ID: order.User.ID}
	err = user.getUser(app.DB)

	if err == nil {
		order.User = filterUserData(&user)
	}

	order.Pharmacy.getPharmacy(app.DB)

	respondWithJSON(w, http.StatusOK, order)
}

func (a *App) getOrders(w http.ResponseWriter, r *http.Request) {

	orders := []Order{}

	rows, err := getOrders(app.DB)

	if err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	for rows.Next() {

		var o Order

		err := rows.Scan(&o.ID, &o.Pharmacy.ID, &o.User.ID, &o.Note, &o.ContactPerson, &o.Processed)

		if err != nil {
			log.Println(err)
			return
		}

		err = o.getOrderMedics(app.DB)

		if err != nil {
			log.Printf(err.Error())
			return
		}

		user := User{ID: o.User.ID}
		err = user.getUser(app.DB)

		if err == nil {
			o.User = filterUserData(&user)
		}

		o.Pharmacy.getPharmacy(app.DB)

		orders = append(orders, o)
	}

	respondWithJSON(w, http.StatusOK, orders)
}

func (a *App) deleteOrder(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	order := Order{ID: id}

	if err := order.deleteOrder(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}
