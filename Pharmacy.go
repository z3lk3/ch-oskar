package main

import (
	"database/sql"
	"fmt"
	"net/url"
)

type Pharmacy struct {
	ID      int    `json:"id,pharmacy_id"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Phone   string `json:"phone"`
	Email   string `json:"email"`
	Note    string `json:"note"`
}

func (p *Pharmacy) createPharmacy(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO pharmacies(name, address, phone, email, note) VALUES(?, ?, ?, ?, ?)")
	_, err := db.Exec(statement, p.Name, p.Address, p.Phone, p.Email, p.Note)

	if err != nil {
		return err
	}

	err = db.QueryRow(`SELECT LAST_INSERT_ID()`).Scan(&p.ID)

	if err != nil {
		return err
	}

	return nil
}

func (p *Pharmacy) getPharmacy(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT name, address, phone, email, note FROM pharmacies WHERE id=?")
	return db.QueryRow(statement, p.ID).Scan(&p.Name, &p.Address, &p.Phone, &p.Email, &p.Note)
}

func (p *Pharmacy) updatePharmacy(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE pharmacies SET name=?, address=?, phone=?, email=?, note=? WHERE id=?")
	_, err := db.Exec(statement, p.Name, p.Address, p.Phone, p.Email, p.Note, p.ID)
	return err
}

func (p *Pharmacy) deletePharmacy(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM pharmacies WHERE id=?")
	_, err := db.Exec(statement, p.ID)
	return err
}

func getPharmacies(db *sql.DB) ([]Pharmacy, error) {
	statement := fmt.Sprintf("SELECT id, name, address, phone, email, note FROM pharmacies")
	rows, err := db.Query(statement)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	pharmacies := []Pharmacy{}

	for rows.Next() {
		var p Pharmacy
		if err := rows.Scan(&p.ID, &p.Name, &p.Address, &p.Phone, &p.Email, &p.Note); err != nil {
			return nil, err
		}

		pharmacies = append(pharmacies, p)
	}

	return pharmacies, nil
}


func (p *Pharmacy) validate() url.Values {
	errs := url.Values{}

	if p.Name == "" {
		errs.Add("name", "Name is required!")
	}

	if p.Address == "" {
		errs.Add("address", "Address is required!")
	}

	return errs
}
