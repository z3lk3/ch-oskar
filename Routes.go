package main

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"net/http"
	"github.com/rs/cors"
)

var handler http.Handler

func (a *App) initializeRoutes() {

	a.Router = mux.NewRouter()
	handler = cors.AllowAll().Handler(a.Router)

	// AUTH ROUTES
	a.Router.HandleFunc("/login", LoginHandler).Methods("POST")
	a.Router.HandleFunc("/signup", RegisterHandler).Methods("POST")
	a.Router.HandleFunc("/logout", LogoutHandler).Methods("POST", "GET")

	// FILES
	a.Router.Handle("/files/{rest}", http.StripPrefix("/files/", http.FileServer(http.Dir("./files/"))))


	///////////////////////////
	//	USER Routes
	///////////////////////////

	a.Router.Handle("/user", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.createUser)),
	)).Methods("POST")

	a.Router.Handle("/user", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getProfile)),
	)).Methods("GET")

	a.Router.Handle("/user/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getUser)),
	)).Methods("GET")

	a.Router.Handle("/users", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getUsers)),
	)).Methods("GET")

	a.Router.Handle("/user/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.updateUser)),
	)).Methods("PUT")

	a.Router.Handle("/user/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.deleteUser)),
	)).Methods("DELETE")


	///////////////////////////
	//	PHARMACY Routes
	///////////////////////////

	a.Router.Handle("/pharmacy", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.createPharmacy)),
	)).Methods("POST")

	a.Router.Handle("/pharmacy", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getPharmacies)),
	)).Methods("GET")

	a.Router.Handle("/pharmacy/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getPharmacy)),
	)).Methods("GET")

	a.Router.Handle("/pharmacy/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.updatePharmacy)),
	)).Methods("PUT")

	a.Router.Handle("/pharmacy/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.deletePharmacy)),
	)).Methods("DELETE")


	///////////////////////////
	//	TASK Routes
	///////////////////////////

	a.Router.Handle("/task", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.createTask)),
	)).Methods("POST")

	a.Router.Handle("/task", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getTasks)),
	)).Methods("GET")

	a.Router.Handle("/task/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getTask)),
	)).Methods("GET")

	a.Router.Handle("/task/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.updateTask)),
	)).Methods("PUT")

	a.Router.Handle("/task/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.deleteTask)),
	)).Methods("DELETE")


	///////////////////////////
	//	WHOLESALER Routes
	///////////////////////////

	a.Router.Handle("/wholesaler", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.createWholesaler)),
	)).Methods("POST")

	a.Router.Handle("/wholesaler", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getWholesalers)),
	)).Methods("GET")

	a.Router.Handle("/wholesaler/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getWholesaler)),
	)).Methods("GET")

	a.Router.Handle("/wholesaler/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.updateWholesaler)),
	)).Methods("PUT")

	a.Router.Handle("/wholesaler/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.deleteWholesaler)),
	)).Methods("DELETE")


	///////////////////////////
	// WHOLESALER MEDIC Routes
	///////////////////////////

	a.Router.Handle("/wholesaler/{id:[0-9]+}/medic", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.createMedic)),
	)).Methods("POST")

	a.Router.Handle("/medics", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getMedicsAndWholesalers)),
	)).Methods("GET")

	a.Router.Handle("/medic/{id}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getMedicByID)),
	)).Methods("GET")

	a.Router.Handle("/wholesaler/{id:[0-9]+}/medic", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getMedicsByWholesalerID)),
	)).Methods("GET")

	a.Router.PathPrefix("/wholesaler/{id:[0-9]+}").Subrouter().Handle("/medic/{medicid:[0-9]+}",
		negroni.New(
			negroni.HandlerFunc(adminMiddleware),
			negroni.Wrap(http.HandlerFunc(a.updateMedic)),
		)).Methods("PUT")

	a.Router.PathPrefix("/wholesaler/{id:[0-9]+}").Subrouter().Handle("/medic/{medicid:[0-9]+}",
		negroni.New(
			negroni.HandlerFunc(adminMiddleware),
			negroni.Wrap(http.HandlerFunc(a.deleteMedic)),
		)).Methods("DELETE")


	///////////////////////////
	//	ORDER Routes
	///////////////////////////

	a.Router.Handle("/order", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getOrders)),
	)).Methods("GET")

	a.Router.Handle("/order/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getOrder)),
	)).Methods("GET")

	a.Router.Handle("/order/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.deleteOrder)),
	)).Methods("DELETE")


	///////////////////////////
	//	LOCATION Routes
	///////////////////////////

	a.Router.Handle("/location", negroni.New(
		negroni.HandlerFunc(userMiddleware),
		negroni.Wrap(http.HandlerFunc(a.createLocation)),
	)).Methods("POST")

	a.Router.Handle("/location/{id:[0-9]+}", negroni.New(
		negroni.HandlerFunc(adminMiddleware),
		negroni.Wrap(http.HandlerFunc(a.getUserLocations)),
	)).Methods("GET")

}
