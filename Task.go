package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
)

type Task struct {
	ID       int      `json:"id"`
	Done     bool     `json:"done"`
	User     RESTUser `json:"user"`
	Pharmacy Pharmacy `json:"pharmacy"`
	Type     Type     `json:"type"`
	Date     string   `json:"date"`
}

type Type struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type DBTask struct {
	Done       bool   `json:"done"`
	UserID     int    `json:"user_id"`
	PharmacyID int    `json:"pharmacy_id"`
	TypeID     int    `json:"type_id"`
	Date       string `json:"date"`
}

func (t *Task) createTask(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO tasks(done, user_id, pharmacy_id, type_id, date) VALUES(?, ?, ?, ?, ?)")

	log.Println("Model task: ", t)
	_, err := db.Exec(statement, t.Done, t.User.ID, t.Pharmacy.ID, t.Type.ID, t.Date)

	if err != nil {
		return err
	}

	err = db.QueryRow(`SELECT LAST_INSERT_ID()`).Scan(&t.ID)

	if err != nil {
		return err
	}

	return nil
}

func (t *Task) getTask(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT t.done, t.user_id, t.pharmacy_id, t.date, tt.id, tt.name FROM tasks t " +
		"JOIN task_type tt ON t.type_id = tt.id " +
		"WHERE t.id=?")

	return db.QueryRow(statement, t.ID).Scan(&t.Done, &t.User.ID, &t.Pharmacy.ID, &t.Date, &t.Type.ID, &t.Type.Name)
}

func (t *Task) updateTask(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE tasks SET done=?, user_id=?, pharmacy_id=?, type_id=?, date=? WHERE id=?")
	_, err := db.Exec(statement, t.Done, t.User.ID, t.Pharmacy.ID, t.Type.ID, t.Date, t.ID)
	return err
}

func (t *Task) deleteTask(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM tasks WHERE id=?")
	_, err := db.Exec(statement, t.ID)
	return err
}

func getTasks(db *sql.DB, id int) ([]Task, error) {
	statement := fmt.Sprintf("SELECT t.id, t.pharmacy_id, t.done, t.date, tt.id, tt.name, t.user_id FROM tasks t " +
		"JOIN task_type tt ON t.type_id = tt.id WHERE t.user_id=?")
	rows, err := db.Query(statement, id)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	tasks := []Task{}

	for rows.Next() {
		var t Task

		if err := rows.Scan(&t.ID, &t.Pharmacy.ID, &t.Done, &t.Date, &t.Type.ID, &t.Type.Name, &t.User.ID); err != nil {
			return nil, err
		}

		user := User{ID:t.User.ID}

		if err = user.getUser(app.DB); err != nil {
			return nil, err
		}

		t.User = filterUserData(&user)

		if err = t.Pharmacy.getPharmacy(app.DB); err != nil {
			return nil, err
		}

		tasks = append(tasks, t)
	}

	return tasks, nil
}

func (t *Type) getTypeName(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT name FROM task_type WHERE id=?")
	return db.QueryRow(statement, t.ID).Scan(&t.Name)
}

func (t *Task) validate() url.Values {
	errs := url.Values{}

	if t.Pharmacy.ID == 0 {
		errs.Add("pharmacy_id", "Pharmacy ID is required!")
	}

	if t.User.ID == 0 {
		errs.Add("user_id", "User ID is required!")
	}

	if t.Type.ID == 0 {
		errs.Add("type_id", "Type ID is required!")
	}

	if t.Date == "" {
		errs.Add("date", "Date is required!")
	}

	return errs
}