package main

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func (a *App) getTasks(w http.ResponseWriter, r *http.Request) {

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	tasks, err := getTasks(app.DB, user.ID)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, tasks)
}

func (a *App) createTask(w http.ResponseWriter, r *http.Request) {
	var t Task
	var dbTask DBTask
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	if err := decoder.Decode(&dbTask); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	log.Println("DB task: ", dbTask)

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	dbTask.convertToTask(&t)

	log.Println("Converted task: ", t)

	t.getMissingData()

	// permit user to change task owner
	if user.Role.ID != 1 {
		t.User.ID = user.ID
	}

	validationErrors := t.validate()
	if len(validationErrors) > 0 {
		validationFailResponse(w, validationErrors)
		return
	}

	if err := t.createTask(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, t)
}

func (a *App) getTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	t := Task{ID: id}

	if err := t.getTask(app.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Task not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	t.getMissingData()

	respondWithJSON(w, http.StatusOK, t)
}

func (a *App) updateTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	var t Task
	var dbTask DBTask

	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	if err := decoder.Decode(&dbTask); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	dbTask.convertToTask(&t)

	t.ID = id

	t.getMissingData()

	if t.User.ID == user.ID || user.Role.ID == 1 {

		validationErrors := t.validate()
		if len(validationErrors) > 0 {
			validationFailResponse(w, validationErrors)
			return
		}

		if err := t.updateTask(app.DB); err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		respondWithJSON(w, http.StatusOK, t)
	} else {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
	}
	return
}

func (a *App) deleteTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	t := Task{ID: id}
	t.getTask(app.DB)
	t.getMissingData()

	log.Println("Taks: ", t)

	if t.User.ID == user.ID || user.Role.ID == 1 {
		if err := t.deleteTask(app.DB); err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
	} else {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
	}
	return

}

func (dbt *DBTask) convertToTask(t *Task) {

	t.Done = dbt.Done
	t.User.ID = dbt.UserID
	t.Pharmacy.ID = dbt.PharmacyID
	t.Type.ID = dbt.TypeID
	t.Date = dbt.Date
}

func (t *Task) getMissingData() {
	if err := t.Pharmacy.getPharmacy(app.DB); err != nil {
		log.Println(err.Error())
	}

	user := User{ID:t.User.ID}
	if err := user.getUser(app.DB); err != nil {
		log.Println(err.Error())
	}

	t.User = filterUserData(&user)

	if err := t.Type.getTypeName(app.DB); err != nil {
		log.Println(err.Error())
	}
}
