package main

import (
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)


func saveFile(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader) string {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return ""
	}

	err = ioutil.WriteFile("./files/"+handle.Filename, data, 0666)
	if err != nil {
		fmt.Fprintf(w, "%v", err)
		return ""
	}

	return handle.Filename
}

func getAndUploadFileFromRequest(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader) string {
	filename := ""

	mimeType := handle.Header.Get("Content-Type")
	switch mimeType {
	case "image/jpeg", "image/png", "application/pdf":
		filename = saveFile(w, file, handle)
	default:
		return ""
	}
	return filename
}
