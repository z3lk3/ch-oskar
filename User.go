package main

import (
	"database/sql"
	"fmt"
	"net/url"
)

type User struct {
	ID            int     `json:"id,user_id"`
	Email         string  `json:"email"`
	Password      string  `json:"password"`
	FirstName     string  `json:"first_name"`
	LastName      string  `json:"last_name"`
	Role          Role    `json:"role"`
	RememberToken string  `json:"remember_token"`
}

type RESTUser struct {
	ID        int    `json:"id,user_id"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Role      string `json:"role"`
}

type DBUser struct {
	ID        int    `json:"id,user_id"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	RoleID    int    `json:"role_id"`
}

type Role struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (u *User) getUser(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT u.email, u.password, u.first_name, u.last_name, r.id, r.name FROM users u JOIN roles r ON u.role_id = r.id WHERE u.id=?")
	return db.QueryRow(statement, u.ID).Scan(&u.Email, &u.Password, &u.FirstName, &u.LastName, &u.Role.ID, &u.Role.Name)
}

func (u *User) updateUser(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE users SET email=?, password=?, first_name=?, last_name=?, role_id=? WHERE id=?")
	_, err := db.Exec(statement, u.Email, u.Password, u.FirstName, u.LastName, u.Role.ID, u.ID)
	return err
}

func (u *User) deleteUser(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM users WHERE id=?")
	_, err := db.Exec(statement, u.ID)
	return err
}

func (u *User) createUser(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO users(email, password, first_name, last_name, role_id) VALUES(?, ?, ?, ?, ?)")
	_, err := db.Exec(statement, u.Email, u.Password, u.FirstName, u.LastName, u.Role.ID)

	if err != nil {
		return err
	}

	err = db.QueryRow(`SELECT LAST_INSERT_ID()`).Scan(&u.ID)

	if err != nil {
		return err
	}

	return nil
}

func (u *User) findByEmail(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT u.id, u.password, u.first_name, u.last_name, r.id, r.name FROM users u JOIN roles r ON u.role_id = r.id WHERE u.email=?")
	return db.QueryRow(statement, u.Email).Scan(&u.ID, &u.Password, &u.FirstName, &u.LastName, &u.Role.ID, &u.Role.Name)
}

func getUsers(db *sql.DB, offset, count int) ([]User, error) {
	statement := fmt.Sprintf("SELECT u.id, u.password, u.first_name, u.last_name, r.id, r.name FROM users u JOIN roles r ON u.role_id = r.id WHERE u.is_deleted=0 LIMIT ? OFFSET ?")
	rows, err := db.Query(statement, count, offset)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	users := []User{}

	for rows.Next() {
		var u User
		if err := rows.Scan(&u.ID, &u.Email, &u.FirstName, &u.LastName, &u.Role.ID, &u.Role.Name); err != nil {
			return nil, err
		}

		users = append(users, u)
	}

	return users, nil
}

func (u *User) updateToken(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE users SET remember_token=? WHERE id=?")
	_, err := db.Exec(statement, u.RememberToken, u.ID)
	return err
}

func (u *User) removeToken(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE users SET remember_token=NULL WHERE id=?")
	_, err := db.Exec(statement, u.ID)
	return err
}

func (u *User) getUserByToken(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT u.ID, u.email, u.password, u.first_name, u.last_name, r.id, r.name FROM users u JOIN roles r ON u.role_id = r.id WHERE u.remember_token=? AND is_deleted=0")
	return db.QueryRow(statement, u.RememberToken).Scan(&u.ID, &u.Email, &u.Password, &u.FirstName, &u.LastName, &u.Role.ID, &u.Role.Name)
}

func (u *User) getRoleByID(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT name FROM roles WHERE id=?")
	return db.QueryRow(statement, u.Role.ID).Scan(&u.Role.Name)
}

func (u *User) validate() url.Values {
	errs := url.Values{}

	if u.FirstName == "" {
		errs.Add("first_name", "First name is required!")
	}

	if u.LastName == "" {
		errs.Add("last_name", "Last name is required!")
	}

	if u.Email == "" {
		errs.Add("email", "Email is required!")
	}

	if u.Password == "" {
		errs.Add("password", "Password is required!")
	}

	return errs
}