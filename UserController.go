package main

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"golang.org/x/crypto/bcrypt"
	"log"
)

func (a *App) createUser(w http.ResponseWriter, r *http.Request) {
	var dbUser DBUser
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	if err := decoder.Decode(&dbUser); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	user := new(User)

	dbUser.convertToUser(user)

	validationErrors := user.validate()
	if len(validationErrors) > 0 {
		validationFailResponse(w, validationErrors)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(dbUser.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err.Error())
	} else {
		user.Password = string(hashedPassword)
	}

	if err := user.createUser(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, user)
}

func (a *App) getProfile(w http.ResponseWriter, r *http.Request) {

	user, err := getUserFromToken(r)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, user)
}

func (a *App) getUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")

		return
	}

	u := User{ID: id}

	if err := u.getUser(app.DB); err != nil {
		switch err {

		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "User not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}

		return
	}

	respondWithJSON(w, http.StatusOK, u)
}

func (a *App) getUsers(w http.ResponseWriter, r *http.Request) {
	count, _ := strconv.Atoi(r.FormValue("count"))
	start, _ := strconv.Atoi(r.FormValue("start"))

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	users, err := getUsers(app.DB, start, count)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, users)
}

func (a *App) updateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	var dbUser DBUser
	dbUser.ID = id

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&dbUser); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	defer r.Body.Close()

	var u User
	dbUser.convertToUser(&u)

	validationErrors := u.validate()
	if len(validationErrors) > 0 {
		validationFailResponse(w, validationErrors)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(dbUser.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err.Error())
	} else {
		u.Password = string(hashedPassword)
	}

	if err := u.updateUser(app.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, u)
}

func (a *App) deleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid ID")
		return
	}

	u := User{ID: id}
	if err := u.deleteUser(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func (dbu *DBUser) convertToUser(u *User) {

	u.ID = dbu.ID
	u.FirstName = dbu.FirstName
	u.LastName = dbu.LastName
	u.Email = dbu.Email
	u.Password = dbu.Password

	u.Role.ID = dbu.RoleID
	u.getRoleByID(app.DB)

}
