package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

var app App

func main() {

	initKeys()

	app.initializeDB("root", "", "oskar_pharm")
	app.initializeRoutes()

	app.Run(":8080")

}

func (a *App) initializeDB(user, password, dbname string) {

	connectionString := fmt.Sprintf("%s:%s@/%s", user, password, dbname)
	var err error

	a.DB, err = sql.Open("mysql", connectionString)

	if err != nil {
		log.Fatal(err)
	}

	log.Println("Now listening...")

}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, handler))
}
